<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Welcome Page</title>
</head>
<body>
<h1>Welcome <%=session.getAttribute("firstname") %> <%=session.getAttribute("lastname") %>!</h1>
	<%
		 String typeOfWork = session.getAttribute("typeOfWork").toString();
		String message;
		
		if(typeOfWork.equals("employee")){
			typeOfWork="Welcome employer. You may now start browsing applicant profiles.";
		}else if(typeOfWork.equals("applicant")){
			typeOfWork="Welcome applicant. You may now start looking for your career opportunity.";
		}
	%>
	<%=typeOfWork %>
</body>
</html>
